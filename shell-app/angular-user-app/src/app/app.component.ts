import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular-user-app';
  users = ["Michel", "Arthur", "Théo", "Boby"];

  ngOnInit(): void {
    console.log("ngOnInit !");

  }
}
