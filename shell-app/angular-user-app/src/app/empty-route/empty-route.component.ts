import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-route',
  template: './empty.html'
})
export class EmptyRouteComponent {}
