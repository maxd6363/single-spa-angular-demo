# Single SPA - Demo

## Lancer l'application Shell

```
cd shell-app
npm i
npm start
```

## Lancer les micro applications

### Angular

```
cd shell-app/angular-user-app
npm i
npm start
```

### React

```
cd shell-app/react-news-app
npm i
npm start
```

